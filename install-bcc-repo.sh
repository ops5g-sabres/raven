#!/bin/bash

apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4052245BD4284CDD
echo "deb https://repo.iovisor.org/apt/bionic bionic main" > /etc/apt/sources.list.d/ioviser.list
apt update
