#!/bin/bash

set -e
set -x

rm -rf build/RPMS
docker $BUILD_ARGS build -t raven-builder-f33 -f fedora/Dockerfile .
docker create --name rvnbuild raven-builder-f33
mkdir -p build
docker cp rvnbuild:/root/rpmbuild/RPMS build/RPMS
docker rm -f rvnbuild

