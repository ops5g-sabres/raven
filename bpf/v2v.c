// SPDX-License-Identifier: GPL-2.0
/* Copyright(c) 2019 - The Raven Authors */

#define KBUILD_MODNAME "v2v"
#include <linux/bpf.h>
#include "bpf_helpers.h"

BPF_TABLE("array", int, int, peer_map, 1);

int v2v_prog(struct xdp_md *ctx)
{

  int key = 0;
  int *peer = peer_map.lookup(&key);
  if (!peer) {
    return XDP_PASS;
  }

  return bpf_redirect(*peer, 0);
}
