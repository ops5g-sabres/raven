module gitlab.com/rygoo/raven

go 1.12

require (
	github.com/fatih/color v1.7.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/gofrs/flock v0.7.1
	github.com/iovisor/gobpf v0.0.0-20210109143822-fb892541d416
	github.com/libvirt/libvirt-go v5.0.0+incompatible
	github.com/libvirt/libvirt-go-xml v6.6.0+incompatible
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mergetb/go-ping v0.0.0-20181106132940-7b3a8e3ebce2
	github.com/sirupsen/logrus v1.4.0
	github.com/spf13/cobra v0.0.3
	gitlab.com/mergetb/tech/rtnl v0.1.8
	gitlab.com/mergetb/xir v0.0.0-20190305161030-8a5d5519d3b8
)
