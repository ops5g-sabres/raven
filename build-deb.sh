#!/bin/bash

set -e

TARGET=${TARGET:-amd64}
DEBUILD_ARGS=${DEBUILD_ARGS:-""}

rm -f build/raven*.build*
rm -f build/raven*.change
rm -f build/raven*.deb

debuild -e V=1 -e prefix=/usr -e arch=amd64 $DEBUILD_ARGS -aamd64 -i -us -uc -b

mv ../raven*.build* build/
mv ../raven*.changes build/
mv ../raven*.deb build/
