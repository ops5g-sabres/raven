lnk = Link("a", 1, "b", 1) 
lnk.v2v = true

topo = {
  name: "rvn-test",
  subnetoffset: 99,
  nodes: [deb("a"), deb("b")],
  links: [lnk]
}

function deb(name) {
  return {
    name: name,
    image: "debian-buster",
    cpu: { cores: 2 },
  }
}
