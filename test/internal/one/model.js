topo = {
  name: "rvn-test",
  subnetoffset: 99,
  nodes: [deb("test")]
}

function deb(name) {
  return {
    name: name,
    image: "debian-buster",
    cpu: { cores: 2 },
  }
}
